**Test project for Livesport**

**Stack**
- mocha
- node.js
- typescript
- docker

**Build**

docker build -t livesport_image .

**Run**

docker run -p 3000:3000 livesport_image

**TODO**

Responding to project growth add linter, swagger, logger, error objects etc.
