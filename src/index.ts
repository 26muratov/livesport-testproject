import express = require('express');
import bodyParser = require('body-parser');

import router = require('./routes');

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());

app.use('/', router);

const server = app.listen(port, () => {
  console.log(`App is listening on port ${port}`);
  console.log('Press CTRL-C to stop\n');
});

process.on('SIGINT', () => {
  console.info('SIGINT signal received.');
  server.close(() => {
    console.log('Http server closed.');
  });
});

export default server;
