import { parse } from '../utils/parser';

class ParserController {
  parseRequest(fields: Array<string>) {
    let result = {};
    fields.forEach(field => {
      result = parse(field, result);
    });
    return result;
  }
}

export const parserController: ParserController = new ParserController();
