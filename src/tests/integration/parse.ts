const supertest = require('supertest');
const { expect } = require('chai');

describe('/parse', () => {
  it('202 for succe.', async () => {
    return supertest('localhost:3000')
      .post('/parse')
      .set('Content-Type', 'application/json')
      .send({
        fields: ['event.home.name', 'event.home.score']
      })
      .expect(202)
  });

  it('422 for invalid data type', async () => {
    return supertest('localhost:3000')
      .post('/parse')
      .set('Content-Type', 'application/json')
      .send({fields: 'test.project'})
      .expect(422);
  });

  it('422 for epty body', async () => {
    return supertest('localhost:3000')
      .post('/parse')
      .set('Content-Type', 'application/json')
      .send({})
      .expect(422);
  });
});
