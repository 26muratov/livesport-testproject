
import { parse } from '../../utils/parser';
import * as  _ from 'lodash';
import { expect } from 'chai';

describe('Parser tests', () => {
  it('Should return correct answer', () => {
    let sample = {
      expect: {
        this: {
          done: null
        }
      }
    }
    let testObject = parse('expect.this.done', {});
    expect(_.isEqual(testObject, sample));
  });
});
