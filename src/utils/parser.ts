export function parse(data: string, root: Object): Object {
  let rootElem: string;
  
  if (data.indexOf('.') > 0) {
    rootElem = data.substr(0, data.indexOf('.'))
    if (!root.hasOwnProperty(rootElem)) root[rootElem] = {};
    let tmp = parse(data.substr(data.indexOf(rootElem) + rootElem.length + 1), root[rootElem]);
    root[rootElem] = {...tmp, ...root[rootElem]};
  } else {
    if (!root.hasOwnProperty(data)) root[data] = null;
    return root;
  }
  return root;
}