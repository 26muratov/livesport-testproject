import express = require('express');

export function validate(req: express.Request, res: express.Response, next) {
  if (!req.body.fields || !Array.isArray(req.body.fields) ) {
    let error = {
      status: 422,
      message: 'Unprocessable entity! Provide array of strings please!'
    };
    res.status(422).send(error);
  }
  next();
}
