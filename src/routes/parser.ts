import { Router, Request, Response } from 'express';
import { parserController } from '../controllers/parser';
import { validate } from '../utils/validator';

const router: Router = Router();

router.post('/', validate, (req: Request, res: Response) => {
  try {
    let result = parserController.parseRequest(req.body.fields);
    res.status(202).send(result);
  } catch {
    let error = {
      status: 500,
      message: 'Internal error!'
    };
    res.status(500).send(error);
  }
});

export const parser: Router = router;
