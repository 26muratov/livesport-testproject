import * as express from 'express';

import { parser } from './parser';

let router: express.Router = express.Router();

router.use('/parse', parser);

export = router;